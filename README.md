![picture](https://gitlab.com/7plus/application/-/raw/main/icon.png)

Unofficial 7plus application, Open source and multi-platform for all platforms to use.

Watch all the latest 7+ content all from the desktop application!

&nbsp;&nbsp;&nbsp;&nbsp;

  You can install 7+ from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/7plus/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/7plus/application/-/releases)

 ### Author
  * Corey Bruce
